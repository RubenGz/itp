<?php
namespace ITP\Gamma\Block;
use \Magento\Framework\View\Element\Template;

class Index extends Template
{
    public function _construct()
    {

    }

    public function getTitle()
    {
        return $this->_scopeConfig->getValue('gamma/groupoffield/display_text');
    }
}
